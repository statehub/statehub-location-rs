//
// Copyright (c) 2022 RepliXio Ltd. All rights reserved.
// Use is subject to license terms.
//

use super::*;
use serde_json as json;

#[test]
fn jsonschema() {
    let objects = vec![
        Location::Aws(aws::AwsRegion::EuCentral1),
        Location::Azure(azure::AzureRegion::NorthEurope),
    ];
    objects.iter().for_each(|object| {
        println!("{}", object);
    });
    assert_eq!(objects.len(), 2);

    let location: Location = "aws/us-east-1".parse().unwrap();
    assert_eq!(location.region(), "us-east-1");
    assert_eq!(location, Location::Aws(aws::AwsRegion::UsEast1));

    println!("{}", json::to_string_pretty(&objects).unwrap());
    let schema = schemars::schema_for!(Location);
    println!("{}", json::to_string_pretty(&schema).unwrap());
}

use schemars::JsonSchema;
use serde::{Deserialize, Serialize};
#[derive(Clone, Debug, Serialize, Deserialize, JsonSchema)]
struct Cluster {
    name: String,
    location: Location,
}

#[test]
fn test_cluster() {
    let cluster = Cluster {
        name: "test".to_string(),
        location: Location::Aws(aws::AwsRegion::EuCentral1),
    };
    println!("{}", json::to_string_pretty(&cluster).unwrap());
    // create clutser from json
    let cluster_json: Cluster = json::from_str(
        r#"
        {
            "name": "test",
            "location": "eu-central-1"
        }
        "#,
    )
    .unwrap();
    println!("{}", json::to_string_pretty(&cluster_json).unwrap());
    assert_eq!(cluster.location, cluster_json.location);
}

#[test]
fn vendor() {
    let objects = vec![
        Location::Aws(aws::AwsRegion::EuCentral1),
        Location::Azure(azure::AzureRegion::NorthEurope),
    ];
    objects.iter().for_each(|object| {
        println!("{}", object);
    });
    assert_eq!(objects.len(), 2);

    assert_eq!(objects[0].vendor(), "aws");
    assert_eq!(objects[1].vendor(), "azure");
}
