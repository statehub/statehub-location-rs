//
// Copyright (c) 2022 RepliXio Ltd. All rights reserved.
// Use is subject to license terms.
//

use super::*;

impl Location {
    pub fn is_aws(&self) -> bool {
        matches!(self, Location::Aws(_))
    }

    pub fn is_azure(&self) -> bool {
        matches!(self, Location::Azure(_))
    }

    pub fn region(&self) -> &str {
        match self {
            Location::Aws(region) => region.as_str(),
            Location::Azure(region) => region.as_str(),
            // Location::Gcp(region) => region.as_str(),
        }
    }

    pub fn vendor(&self) -> &str {
        match self {
            Location::Aws(_) => "aws",
            Location::Azure(_) => "azure",
        }
    }
}

impl fmt::Display for Location {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Aws(region) => f.write_fmt(format_args!("{:#}", region)),
            Self::Azure(region) => f.write_fmt(format_args!("{:#}", region)),
        }
    }
}

impl From<aws::AwsRegion> for Location {
    fn from(region: aws::AwsRegion) -> Self {
        Self::Aws(region)
    }
}

impl From<azure::AzureRegion> for Location {
    fn from(region: azure::AzureRegion) -> Self {
        Self::Azure(region)
    }
}

impl str::FromStr for Location {
    type Err = ParseError;

    fn from_str(text: &str) -> Result<Self, Self::Err> {
        let aws = text.parse::<aws::AwsRegion>();
        let azure = text.parse::<azure::AzureRegion>();
        // let gcp = text.parse::<v1::GcpRegion>();

        match (aws, azure) {
            (Ok(aws), Err(_)) => Ok(Self::Aws(aws)),
            (Err(_), Ok(azure)) => Ok(Self::Azure(azure)),
            (Ok(aws), Ok(azure)) => Err(ParseError::ambiguous(aws, azure)),
            (Err(aws), Err(azure)) => Err(ParseError::unknown(aws, azure)),
        }
    }
}

#[cfg(feature = "jsonschema")]
impl schemars::JsonSchema for Location {
    fn schema_name() -> String {
        String::from("Location")
    }

    fn json_schema(_gen: &mut schemars::gen::SchemaGenerator) -> schemars::schema::Schema {
        use enum_iterator::IntoEnumIterator;

        let aws = aws::AwsRegion::into_enum_iter().map(|region| format!("{:#}", region));
        let azure = azure::AzureRegion::into_enum_iter().map(|region| format!("{:#}", region));
        let enum_values = aws.chain(azure).map(Into::into).collect();

        schemars::schema::Schema::Object(schemars::schema::SchemaObject {
            instance_type: Some(schemars::schema::InstanceType::String.into()),
            enum_values: Some(enum_values),
            ..schemars::schema::SchemaObject::default()
        })
    }
}

#[cfg(feature = "graphql")]
#[juniper::graphql_scalar]
impl<S> juniper::GraphQLScalar for Location
where
    S: juniper::ScalarValue,
{
    fn resolve(&self) -> juniper::Value {
        juniper::Value::scalar(self.to_string())
    }

    fn from_input_value(value: &InputValue) -> Option<Self> {
        value.as_string_value()?.parse().ok()
    }

    // Define how to parse a string value.
    fn from_str<'a>(value: ScalarToken<'a>) -> juniper::ParseScalarResult<'a, S> {
        <String as juniper::ParseScalarValue<S>>::from_str(value)
    }
}
