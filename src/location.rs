//
// Copyright (c) 2022 RepliXio Ltd. All rights reserved.
// Use is subject to license terms.
//

use std::fmt;
use std::str;

use serde_with::{DeserializeFromStr, SerializeDisplay};

use thiserror::Error;

use crate::aws;
use crate::azure;

mod impls;
#[cfg(test)]
#[cfg(feature = "jsonschema")]
mod tests;

pub trait CloudLocation {
    const VENDOR: &'static str;
    const VENDOR_PREFIX: &'static str;
    fn as_str(&self) -> &'static str;
}

/// Generic Cloud Location
#[derive(
    Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, SerializeDisplay, DeserializeFromStr,
)]
pub enum Location {
    Aws(aws::AwsRegion),
    Azure(azure::AzureRegion),
}

#[derive(Error, Debug)]
pub enum ParseError {
    #[error("Ambiguous region, use either {0}")]
    AmbiguousLocation(String),
    #[error("Unknown region: {0}")]
    UnknownLocation(String),
}

impl ParseError {
    fn ambiguous(aws: aws::AwsRegion, azure: azure::AzureRegion) -> Self {
        Self::AmbiguousLocation(format!("{:#} or {:#}", aws, azure))
    }

    fn unknown(aws: InvalidLocation, azure: InvalidLocation) -> Self {
        Self::UnknownLocation(format!("{} or {}", aws, azure))
    }
}

#[derive(Debug, Error)]
#[error(r#"Invalid {vendor} region "{region}""#)]
pub struct InvalidLocation {
    vendor: String,
    region: String,
}

impl InvalidLocation {
    pub fn new(vendor: &str, region: &str) -> Self {
        let vendor = vendor.to_string();
        let region = region.to_string();
        Self { vendor, region }
    }
}
